/**
 * Created by amritha on 10/29/2016.
 */


$(document).ready(function() {
    // Prevent the form from resetting continuously on submission
    $("form").submit(function(event){
        event.preventDefault();
    });
    // Validate the user input
    $('#loan_form').bootstrapValidator({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                loanAmount: {
                    validators: {
                        regexp: {
                            regexp: /^\d*(\.\d+)?$/i,
                            message: 'The loan amount can contain numbers only',
                        },
                        notEmpty: {
                            message: 'Please supply valid loan amount'
                        }
                    }
                },
                propValue: {
                    validators: {
                        regexp: {
                            regexp: /^\d*(\.\d+)?$/i,
                            message: 'The property value can contain numbers only'
                        },
                        notEmpty: {
                            message: 'Please supply valid property value'
                        }
                    }
                },
                ssnValue: {
                    validators: {
                        regexp: {
                            regexp: /^(?!000)(?!666)(?!9)(\d{9})$/i,
                            message: 'SSN can contain numbers only'
                        },
                        notEmpty: {
                            message: 'Please supply valid SSN'
                        },
                    }
                }
            }
        })
        // Enable the submit button when all fields are valid
        .on('success.field.fv', function(e, data) {
            if (data.fv.getInvalidFields().length > 0) {    // There is invalid field
                data.fv.disableSubmitButtons(true);
            }
        });
});


function postForm(){
    var formData = {
        'loan_Amount'  : $('input[name=loanAmount]').val(),
        'prop_Val'     : $('input[name=propValue]').val(),
        'ssn_Value'    : $('input[name=ssnValue]').val()
    };
    // Process the data only when all required fields are populated
    if(formData.loan_Amount && formData.prop_Val && formData.ssn_Value){
        var ltv = (formData.loan_Amount/formData.prop_Val)*100;
        // Generate loan ID based on year, and last 4 digits of SSN
        formData.loanId = "LN"+(new Date().getFullYear())+formData.ssn_Value.substring(5,9);
        var status = null;
        // Notify the result to the user based on LTV
        if(ltv > 40){
            status = "Rejected";
            $.bootstrapGrowl('Loan ID: '+formData.loanId+'     Sorry Loan Not Approved!',
                {type:'warning',
                    align: "center",
                    width: 500,
                    textalign: "center",
                    animate:{
                        enter: 'animate fadeInDown',
                        exit: 'animate fadeOutUp'
                    }
                }
            );
        }
        else {
            status = "Approved";
            $.bootstrapGrowl('Loan ID: '+formData.loanId+'     Congratulations! Loan Approved!',
                {type:'success',
                    align: "center",
                    width: 500,
                    textalign: "center",
                    animate:{
                        enter: 'animate fadeInDown',
                        exit: 'animate fadeOutUp'
                    }
                }
            );
        }
        formData.status = status;
        resetForm();
        saveToStorage('loans', formData);
    }
}

function saveToStorage(name, data){
    // Save the data to local storage
    var temp = JSON.parse(localStorage.getItem(name)) || [];
    temp.push(data);
    localStorage.setItem(name, JSON.stringify(temp));
}

function resetForm(){
    // Once th form is reset, disable the submit button till valid values are entered
    $('#loan_form')[0].reset();
    $('.submit-btn').addClass('disabled');
}

function getLoanStatus(){
    // Check if there is a loan ID that matches with what the user is looking for.
    // Display result accordingly
    var searchLoan = $('input[name=loanId]').val().toLowerCase();
    var temp = JSON.parse(localStorage.getItem('loans')) || [];
    for (var i=0; i < temp.length; i++) {
        if (temp[i].loanId.toLowerCase() === searchLoan) {
            $('.displayStatus').html( "Loan Status for ID <em><strong>"+searchLoan.toUpperCase()+"</strong></em> is: "+temp[i].status.toUpperCase() );
            break;
        }
        resetLoanStatus("Could not find status for the loan ID: "+searchLoan.toUpperCase());
    }
}

function resetLoanStatus(message){
    // Display loan search result based on loan ID
    $('.displayStatus').html(message);
    $('#search_form')[0].reset();
}